#!/bin/bash

cur="$(readlink -f "$0" | xargs dirname)"

if type "horas-hamster" 2>&1 >/dev/null; then
  echo "Las sutty-tools ya están instaladas!"
  exit 1
fi

echo "Agregando sutty-tools a la variable de entorno PATH"

for rc in "${HOME}/.bashrc" "${HOME}/.bash_profile"; do
  test -f "${rc}" || continue

  echo "${cur}:$PATH" >> "${rc}"
  source "${rc}"
done

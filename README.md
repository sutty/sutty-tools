# Herramientas varias de Sutty

## Instalar

Clonar este repositorio y ejecutar el _script_ de instalación.

Una sola vez:

```bash
git clone https://0xacab.org/sutty/sutty-tools.git
cd sutty-tools
./install-sutty-tools.sh
```

A partir de ahora se puede ejecutar cualquier sutty-tool desde cualquier
terminal.

## `horas-hamster`

Calcula las horas de trabajo en la categoría "@sutty" de Hamster y las
envía por Zulip a #Tezoro.  Ejecutar este programa el 21 de cada mes.
